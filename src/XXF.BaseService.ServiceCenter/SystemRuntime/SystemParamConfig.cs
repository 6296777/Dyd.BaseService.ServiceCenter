﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.SystemRuntime
{
    public class SystemParamConfig
    {
        /// <summary>
        /// 内部Redis发布订阅通讯通道名
        /// </summary>
        public static string Redis_Channel = "ServiceCenter.Redis.Channel";
        /// <summary>
        /// Redis发布订阅通讯通道注册失败间隔重试时间
        /// </summary>
        public static int Redis_Subscribe_FailConnect_ReConnect_Every_Time = 5;
        /// <summary>
        /// ZooKeeper注册节点失败间隔重试时间(zk本身可实现自动重连)
        /// </summary>
        public static int ZK_RegisterNode_FailConnect_ReConnect_Every_Time = 10;
        /// <summary>
        /// 服务心跳到服务中心
        /// </summary>
        public static int Node_HeatBeat_Every_Time = 10;
        /// <summary>
        /// 服务心跳到服务中心超时时间
        /// </summary>
        public static int Node_HeatBeat_Time_Out = Node_HeatBeat_Every_Time+5;
        /// <summary>
        /// 客户端心跳到服务中心
        /// </summary>
        public static int Client_HeatBeat_Every_Time = 10;


        public static string ServiceRedisChannel(int serviceid)
        {
            return Redis_Channel + "." + "ServiceID_" + serviceid;
        }
    }
}
