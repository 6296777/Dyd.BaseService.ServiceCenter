﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Service.OpenDoc
{
    /// <summary>
    /// 服务文档特性
    /// </summary>
    public class ServiceDocAttribute:OpenDocAttribute
    {
        public ServiceDocAttribute() { }
        public ServiceDocAttribute(string text, string description)
            : base(text, description)
        {
 
        }
    }
}
