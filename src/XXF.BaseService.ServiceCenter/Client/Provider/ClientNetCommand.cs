﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Client.Provider
{
    /// <summary>
    /// 客户端网络通信命令
    /// </summary>
    public class ClientNetCommand
    {
        public EnumClientCommandType CommandType { get; set; }
        public string ServiceNameSpace { get; set; }
    }

    /// <summary>
    /// 网络命令类型
    /// </summary>
    public enum EnumClientCommandType
    {
        /// <summary>
        /// 服务更新
        /// </summary>
        ServiceUpdate,
       
    }
}
