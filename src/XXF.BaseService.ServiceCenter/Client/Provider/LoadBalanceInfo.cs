﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Client.Provider
{
    /// <summary>
    /// 负载均衡信息
    /// </summary>
    public class LoadBalanceInfo
    {
        public int Port { get; set; }
        public string Host { get; set; }
    }
}
